export interface INewsDetail {
    _id: string;
    title: string;
    publish_date: string;
    clean_url: string;
    summary: string;
    media: string;
}