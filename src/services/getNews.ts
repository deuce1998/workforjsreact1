import axios, {AxiosResponse, AxiosRequestConfig, Axios} from "axios";

const getNews = (): Promise<AxiosResponse> => {
    const options: AxiosRequestConfig = {
        method: 'GET',
        url: 'https://free-news.p.rapidapi.com/v1/search',
        params: {q: 'Elon Musk', lang: 'en'},
        headers: {
            'X-RapidAPI-Host': 'free-news.p.rapidapi.com',
            'X-RapidAPI-Key': 'f6f28956camsh2402dcbf8683c93p1100cfjsn13c6ed075613'
        }
    };

    return axios.request(options);    
}

export default getNews;