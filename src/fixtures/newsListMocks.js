const newsListMocks = [
    {
        id: 1,
        title: "Новость 1",
        source: 'meduza.com',
        data: '09/03'
    },
    {
        id: 2,
        title: "Новость 2",
        source: 'meduza.com',
        data: '09/03'
    },
    {
        id: 3,
        title: "Новость 3",
        source: 'meduza.com',
        data: '09/03'
    },
    {
        id: 4,
        title: "Новость 4",
        source: 'meduza.com',
        data: '09/03'
    },
    {
        id: 5,
        title: "Новость 5",
        source: 'meduza.com',
        data: '09/03'
    },
    {
        id: 6,
        title: "Новость 6",
        source: 'meduza.com',
        data: '09/03'
    },
    {
        id: 7,
        title: "Новость 7",
        source: 'meduza.com',
        data: '09/03'
    },
    {
        id: 8,
        title: "Новость 8",
        source: 'meduza.com',
        data: '09/03'
    },
    {
        id: 9,
        title: "Новость 9",
        source: 'meduza.com',
        data: '09/03'
    },
    {
        id: 10,
        title: "Новость 10",
        source: 'meduza.com',
        data: '09/03'
    },
]

export default newsListMocks;