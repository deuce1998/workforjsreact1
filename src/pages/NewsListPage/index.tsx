import React, {useState, useEffect}  from "react";
import routeMain from "./routes";

import getNews from "services/getNews";
import PageTitle from "components/PageTitle";
import NewsList from "components/NewsList";

import { INewsDetail } from "types/INewsDetail";

import './styles.scss'

const NewsListPage = () => {
    const [newsList, setNewsList] = useState<INewsDetail[]>([]);

    useEffect(() => {
        getNews().then(response => {
            setNewsList(response.data.articles)
        })
    }, [])

    return (
        <section className="mainPage">
            <PageTitle 
                title={
                    <h2>
                        Всегда <br /> свежие <span>новости</span>
                    </h2>
                }
            />
            {newsList.length > 0 && <NewsList list={newsList}/>}
        </section>
    )

}

export {routeMain};

export default NewsListPage;