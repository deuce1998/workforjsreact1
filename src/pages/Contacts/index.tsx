import React from "react";
import routeMain from "./routes";

import './styles.scss'

const Contacts = () => (
    <section className="contactsPage">
        <div className="primeContent">
            <a href="tel:" >+7 (927) xxx xx-xx</a>
            <div>
                <p>
                    Алексей 
                    Матюшкинs
                </p>
                <a href="email">alexeydeveloper@gmail.com</a>
            </div>
        </div>
        <div className="myPhoto">
            <img src="" alt="" />
        </div>
    </section>
)

export {routeMain};

export default Contacts;