import React from "react";

import { NavLink } from "react-router-dom";

import { routeMain as routeNewsDetail } from "pages/NewsDetail";

import DateView from "components/DateView";

import { INewsDetail } from 'types/INewsDetail';

import './style.scss'

interface INewsItemParams {
    item: INewsDetail;
}




const NewsItem: React.FC<INewsItemParams> = ({item}) => {
    return (
        <NavLink className = "newsItem-wrapper" to={routeNewsDetail(item._id)}>
            <div className = "newsItem">
                <div className = "title">
                    {item.title}
                </div>
                    
            </div>
            <div className = "bottonWrapper">
            <div className = "source">
                    <p>{item.clean_url}</p>
                </div>
            </div>        
        </NavLink>

    )

}

export default NewsItem;