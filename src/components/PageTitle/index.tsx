import React from "react";

import './styles.scss'

interface IPageTitle {
    title: JSX.Element
}



const PageTitle:React.FC<IPageTitle> = ({title}) => <div className="pageTitle">{title}</div>

export default PageTitle;