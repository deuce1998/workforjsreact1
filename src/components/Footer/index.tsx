import React from 'react';
import './styles.scss';

const Footer = () => {
  return (
    <div className="mainFooter">
      <p>
        Новостник <span className="small">SP application</span>
      </p>
      <p>Дипломный проект</p>
      <p><span className="small">Made by Alexey M</span></p>
    </div>
  );
}

export default Footer;
