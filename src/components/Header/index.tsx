import React from 'react';


import { NavLink } from 'react-router-dom';

import  {routeMain as routMainPage} from 'pages/MainPage';
import {routeMain as routNewsList} from 'pages/NewsListPage';
import {routeMain as routContacts} from 'pages/Contacts';

import './styles.scss';

const Header = () => {
  return (
    <header className="mainHeader">
      <div className="title">Новостник</div>
      <nav>
        <NavLink to={routMainPage()} activeClassName={'linkActive'}>
          Главная
        </NavLink>
        <NavLink to={routNewsList()} activeClassName={'linkActive'}>
          Новости
        </NavLink>
        <NavLink to={routContacts()} activeClassName={'linkActive'}>
          Контакты
        </NavLink>
      </nav>
    </header>
  );
}

export default Header;
