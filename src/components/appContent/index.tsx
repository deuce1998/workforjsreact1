import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import MainPage, {routeMain as routMainPage} from 'pages/MainPage';
import NewsListPage, {routeMain as routNewsList} from 'pages/NewsListPage';
import NewsDetail, {routeMain as routNewsDetail} from 'pages/NewsDetail';
import Contacts, {routeMain as routContacts} from 'pages/Contacts';

import Header from 'components/Header';
import Footer from 'components/Footer';
import './styles.scss';

const AppContent = () => {
  return (
    <div className="mainWrapper">
        <Header />
        <main>
          <Switch>
            <Route exact path = {routMainPage()} component = {MainPage}/>
            <Route exact path = {routNewsList()} component = {NewsListPage}/>
            <Route exact path = {routNewsDetail()} component = {NewsDetail}/>
            <Route exact path = {routContacts()} component = {Contacts}/>
            <Redirect
              to={{
                pathname: routMainPage()
              }}
            />
          </Switch>
        </main>
        <Footer />
    </div>
  );
}

export default AppContent;
